Feature: Navigate to Undostres Website

  Scenario: Validate that Undostres webpage is displayed with Recargas Celular feature
    When I navigate to Undostres Webpage
    Then the Undostres home page is displayed

  Scenario: Validate payment screen redirection by selecting operator amount and number for recharge
    When I click on the field "Operador"
    And I select "Telcel" from the "Operador" category
    And I enter the following details on the "Recarga Celular" section
      | Mobile | 5523261151 |
    And I click on the field "Monto de Recarga"
    And I select "10" from the "Monto de Recarga" category
    And I click on "Siguiente" button
    Then the "Payment Form" is displayed

  Scenario: Validate Payment for the 10$ recharge is successful
    When I click on the tab "Tarjeta"
    And I enter the following details on the "Payment Form" section
      | Card Name    | Test             |
      | Card Number  | 4111111111111111 |
      | Expiry Month | 11               |
      | Expiry Year  | 2025             |
      | CVV No.      | 111              |
      | Email        | test@test.com    |
    And I click on "Pagar con Tarjeta $10" button
    Then the login window to access the account is displayed