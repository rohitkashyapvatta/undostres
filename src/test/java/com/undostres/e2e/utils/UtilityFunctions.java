package com.undostres.e2e.utils;

import java.util.Arrays;

public class UtilityFunctions {
    public static String convertStringToHyphenatedWithLowerCase(String actualString) {
        String[] splittedStrings = actualString.split(" ");
        return Arrays.stream(splittedStrings)
                .map(String::toLowerCase)
                .reduce((s1, s2) -> s1.concat("-").concat(s2)).get();
    }
}
