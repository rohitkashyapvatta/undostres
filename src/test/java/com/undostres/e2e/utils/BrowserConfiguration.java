package com.undostres.e2e.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.*;

@AllArgsConstructor
@Getter
public enum BrowserConfiguration {
    DESKTOP(new Rectangle(1600, 1400));

    private final Rectangle windowSize;
}