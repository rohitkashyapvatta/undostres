package com.undostres.e2e.pageObjects;

import com.github.loyada.jdollarx.ElementProperties;
import com.github.loyada.jdollarx.Path;

import static com.github.loyada.jdollarx.BasicPath.*;
import static com.github.loyada.jdollarx.ElementProperties.hasAggregatedTextContaining;
import static com.github.loyada.jdollarx.ElementProperties.hasAnyOfClasses;
import static com.undostres.e2e.step.definition.CommonPath.FIELD_TO_CLASS_MAPPER;

public class CommonPage {

    public static Path getPathOfExpectedFieldName(String fieldName) {
        return input.immediatelyBeforeSibling(label.withTextContaining(fieldName));
    }

    public static Path getPathOfOptionFromCategory(String operatorName, String categoryName) {
        return listItem.that(ElementProperties.hasAttribute("data-name", operatorName.toLowerCase()))
                .inside(unorderedList.that(ElementProperties.contains(label.withTextContaining(categoryName))));
    }

    public static Path getPathOfExpectedTab(String tabName) {
        return anchor.that(hasAnyOfClasses(FIELD_TO_CLASS_MAPPER.get(tabName)))
                .that(hasAggregatedTextContaining(tabName));
    }

    public static Path getExpectedButtonName(String buttonName) {
        return button.withTextContaining(buttonName);
    }
}
