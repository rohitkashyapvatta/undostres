package com.undostres.e2e.pageObjects;

import com.github.loyada.jdollarx.ElementProperties;
import com.github.loyada.jdollarx.Path;

import static com.github.loyada.jdollarx.BasicPath.*;
import static com.undostres.e2e.step.definition.CommonPath.FIELD_TO_CLASS_MAPPER;
import static com.undostres.e2e.utils.UtilityFunctions.convertStringToHyphenatedWithLowerCase;

public class PaymentPage {
    public static Path getInputPathInsideSection(Path parentSection, String key) {
        return input.that(ElementProperties.hasAttribute("name", FIELD_TO_CLASS_MAPPER.get(key))).inside(parentSection);
    }

    public static Path getPathOfForm(String sectionName) {
        return form.that(ElementProperties.hasAttribute("id", convertStringToHyphenatedWithLowerCase(sectionName)));
    }

    public static Path getPathOfLoginModalWindow() {
        return div.that(ElementProperties.hasAnyOfClasses("loginToProceed"));
    }

    public static Path getPathOfFormWithSectionHeading(String sectionName) {
        return form.contains(header1.withTextContaining(sectionName));
    }
}
