package com.undostres.e2e.step.definition;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class CommonPath {

    public static final Map<String, String> FIELD_TO_CLASS_MAPPER =
            ImmutableMap.<String, String>
                    builder()
                    .put("Tarjeta", "select-card")
                    .put("Card Number", "cardno")
                    .put("Card Name", "cardname")
                    .put("Mobile", "mobile")
                    .put("Expiry Month", "expmonth")
                    .put("Expiry Year", "expyear")
                    .put("CVV No.", "cvvno")
                    .put("Email", "txtEmail")
                    .build();

}
