package com.undostres.e2e.step.definition;

import cucumber.api.java.en.Then;
import org.junit.Assert;

import static com.github.loyada.jdollarx.singlebrowser.InBrowserSinglton.driver;

public class HomePageStepDefs {

    public static final String UNDOSTRES = "undostres";

    @Then("^the Undostres home page is displayed$")
    public void validateUndostresHomePageIsDisplayed() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(UNDOSTRES));
    }
}
