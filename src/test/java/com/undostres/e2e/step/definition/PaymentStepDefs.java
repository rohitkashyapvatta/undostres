package com.undostres.e2e.step.definition;

import com.github.loyada.jdollarx.Operations;
import com.github.loyada.jdollarx.Path;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.util.Map;

import static com.github.loyada.jdollarx.singlebrowser.InBrowserSinglton.sendKeys;
import static com.github.loyada.jdollarx.singlebrowser.custommatchers.CustomMatchers.isPresent;
import static com.undostres.e2e.pageObjects.PaymentPage.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class PaymentStepDefs {
    @And("^I enter the following details on the \"([^\"]*)\" section$")
    public void enterTheDetailsOnPaymentForm(String sectionName, Map<String, String> paymentFormFields) {
        Path parentSection = getPathOfForm(sectionName).or(getPathOfFormWithSectionHeading(sectionName));
        paymentFormFields
                .forEach((key, value) -> {
                    try {
                        sendKeys(value).to(getInputPathInsideSection(parentSection, key));
                    } catch (Operations.OperationFailedException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Then("^the \"([^\"]*)\" is displayed$")
    public void validatePaymentScreenIsDisplayed(String expectedScreen) {
        Path expectedPaymentScreen = getPathOfForm(expectedScreen);
        assertThat(expectedPaymentScreen, isPresent());
    }

    @Then("^the login window to access the account is displayed$")
    public void validateLoginWindowToAccessTheAccountIsDisplayed() {
        Path loginAccountModalWindow = getPathOfLoginModalWindow();
        assertThat(loginAccountModalWindow, isPresent());
    }
}
