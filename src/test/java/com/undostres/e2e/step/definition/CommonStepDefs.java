package com.undostres.e2e.step.definition;

import com.github.loyada.jdollarx.Path;
import com.undostres.e2e.config.WebConfigProps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static com.github.loyada.jdollarx.singlebrowser.InBrowserSinglton.clickOn;
import static com.github.loyada.jdollarx.singlebrowser.InBrowserSinglton.driver;
import static com.undostres.e2e.pageObjects.CommonPage.*;

public class CommonStepDefs {

    @Autowired
    private WebConfigProps webConfigProps;

    @When("^I navigate to Undostres Webpage$")
    public void navigateToUndostresWebpage() {
        driver.get(webConfigProps.getBaseUrl());
    }

    @When("^I click on the field \"([^\"]*)\"$")
    public void clickOnTheField(String fieldName) {
        Path expectedFieldName = getPathOfExpectedFieldName(fieldName);
        clickOn(expectedFieldName);
    }


    @And("^I select \"([^\"]*)\" from the \"([^\"]*)\" category$")
    public void selectTheEntity(String operatorName, String categoryName) {
        Path expectedOperatorName = getPathOfOptionFromCategory(operatorName, categoryName);
        clickOn(expectedOperatorName);
    }

    @And("^I click on \"([^\"]*)\" button$")
    public void clickOnButton(String buttonName) {
        Path expectedButtonName = getExpectedButtonName(buttonName);
        clickOn(expectedButtonName);
    }

    @When("^I click on the tab \"([^\"]*)\"$")
    public void clickOnTheTab(String tabName) {
        Path expectedTabName = getPathOfExpectedTab(tabName);
        clickOn(expectedTabName);
    }
}
