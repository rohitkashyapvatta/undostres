package com.undostres.e2e.config;

import com.undostres.e2e.utils.BrowserConfiguration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Value;

import java.awt.*;

import static java.lang.String.format;
import static java.util.logging.Level.WARNING;
import static org.openqa.selenium.logging.LogType.BROWSER;

public class BrowserConfigs {
    private static WebDriver driver;

    public static WebDriver setFireFoxDriverConfig(@Value("${browser.headless}") boolean browserHeadless, @Value("${browser.configuration}") BrowserConfiguration browserConfiguration) {
        WebDriverManager.firefoxdriver().setup();
        Rectangle windowSize = browserConfiguration.getWindowSize();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments(format("--window-size=%s,%s", windowSize.width, windowSize.height));
        if (browserHeadless) {
            firefoxOptions.setHeadless(true);
        }
        driver = new FirefoxDriver(firefoxOptions);


        return driver;
    }

    public static WebDriver setChromeDriverConfig(@Value("${browser.headless}") boolean browserHeadless, @Value("${browser.configuration}") BrowserConfiguration browserConfiguration) {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        if (browserHeadless) {
            chromeOptions.addArguments("headless");
        }
        chromeOptions.addArguments("--disable-gpu");
        Rectangle windowSize = browserConfiguration.getWindowSize();
        chromeOptions.addArguments(
                format("--window-size=%s,%s", windowSize.width, windowSize.height)
        );

        DesiredCapabilities cap = DesiredCapabilities.chrome();
        LoggingPreferences pref = new LoggingPreferences();
        pref.enable(BROWSER, WARNING);
        cap.setCapability(CapabilityType.LOGGING_PREFS, pref);
        chromeOptions.merge(cap);
        driver = new ChromeDriver(chromeOptions);
        return driver;
    }
}
