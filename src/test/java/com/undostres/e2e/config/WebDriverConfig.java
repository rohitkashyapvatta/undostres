package com.undostres.e2e.config;

import com.undostres.e2e.utils.BrowserConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

import static com.undostres.e2e.config.BrowserConfigs.setChromeDriverConfig;
import static com.undostres.e2e.config.BrowserConfigs.setFireFoxDriverConfig;
import static java.util.concurrent.TimeUnit.SECONDS;

@Configuration
@ComponentScan("com.undostres.e2e")
public class WebDriverConfig {
    @Bean
    public WebDriver webDriver(@Value("${browser.browserName}") String browser,
                               @Value("${browser.headless}") boolean browserHeadless,
                               @Value("${browser.configuration}")
                                       BrowserConfiguration browserConfiguration) {
        WebDriver driver = null;
        if (browser.equalsIgnoreCase("chrome")) {
            driver = setChromeDriverConfig(browserHeadless, browserConfiguration);
        } else if (browser.equalsIgnoreCase("firefox")) {
            driver = setFireFoxDriverConfig(browserHeadless, browserConfiguration);
        }

        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(Objects.requireNonNull(driver));
        eventFiringWebDriver.register(new AbstractWebDriverEventListener() {

            @Override
            public void beforeNavigateTo(String url, WebDriver driver) {
                super.beforeNavigateTo(url, driver);
            }
        });
        return eventFiringWebDriver;
    }
}
