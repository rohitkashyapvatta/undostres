# Undostres-end-to-end-tests
This module includes set of 8 end to end tests which will validate some of the features of the website. The tests are fully dynamic with clean reusable code.

#Technology Stack
Leveraging the APIs of Cucumber along with DollarX for efficient Xpath is utilised for meeting the BDD framework guidelines.

#How to RUN the Tests
The pre-requisite for setting up the Test Suite to install JAVA8, MAVEN and other dependencies mentioned in the pom.xml.
After the completion of setup tests can be triggered by running command mvn verify on terminal or simply by running the RunCucumberIt class from src -> test -> java

Steps for Running the tests post cloning the codebase:

1. Import the pom.xml
2. Run Maven Lifecycle -> Maven clean -> Maven Compile -> Maven Install or Maven Verify

The tests run in non-headless mode with dynamic configuration for browser window size.

The headless mode execution can be triggered by changing the flag from application.yml 

#Important notes
The browser configuration are maintained and can be extended for other browsers as well. Currently we are validating the features on Chrome and Firefox browsers.
We can pass the expected browser through the application.yml file and WebDriver Config will instantiate the desired browser.